## v1.4.0 (2022-03-20)

No changes. ⚠️
## v1.5.0 (2022-03-20)

### Bug fix (1 change)

- [Commit feature/LMS-936](soprun/amqp@17712f9a0d0ef8a3c593440800236cad3ecf0cb1) ([merge request](soprun/amqp!21))

### Feature change (1 change)

- [<Commit message subject>](soprun/amqp@ef93c4f64bdc91f6081f1d602dffbe7539100623)

### Other (2 changes)

- [Fixed: Commit message...](soprun/amqp@0f9086d8cfdcbb80ffffcb1002d11236aa97e6c7) ([merge request](soprun/amqp!22))
- [Commit message...](soprun/amqp@a043345a74948d10f143b06f95e2819d57a0025d)

### fix (4 changes)

- [Commit...](soprun/amqp@eadecfa325fca0ed6867da41ec97ecc9bfb88442)
- [INLMS-89: Управление контейнеризацией (Docker, Kubernetes)](soprun/amqp@3e94ee2e7d2d46e9dbbb429d93dd664905c4cbf2)
- [INLMS-89: Управление контейнеризацией (Docker, Kubernetes)](soprun/amqp@87dab5f9c895495c23cb6dfbb5fe326ff890392d)
- [fix(#123): Заголовок](soprun/amqp@cd8b1240d7e192fc2cc31ccf6bad2fc6906ebf46)

### feature (1 change)

- [TASK-123: Заголовок](soprun/amqp@af145a47e9138acd247199d5485befb0d6944da5)
## v1.6.0 (2022-03-20)

### Bug fix (1 change)

- [Commit feature/LMS-936](soprun/amqp@17712f9a0d0ef8a3c593440800236cad3ecf0cb1) ([merge request](soprun/amqp!21))

### Other (1 change)

- [Fixed: Commit message...](soprun/amqp@0f9086d8cfdcbb80ffffcb1002d11236aa97e6c7) ([merge request](soprun/amqp!22))
