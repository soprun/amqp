http://localhost:15672 - rabbitmq

http://localhost:8025 - mailhog

```
Awesome new feature

Some details about this commit

JIRA-Ref: PROJ-123
```

```text
Используем следующие типы коммитов:
build	Сборка проекта или изменения внешних зависимостей
ci	Настройка CI и работа со скриптами
docs	Обновление документации
feat	Добавление нового функционала
fix	Исправление ошибок
perf	Изменения направленные на улучшение производительности
refactor	Правки кода без исправления ошибок или добавления новых функций
revert	Откат на предыдущие коммиты
style	Правки по кодстайлу (табы, отступы, точки, запятые и т.д.)
test	Добавление тестов
```

```text
TASK-123: Заголовок

- Подробное описание (если оно требуется)

Changelog: fix
```

```shell
git config trailer.changelog.key "Changelog"
git config trailer.changelog.ifmissing add
git config trailer.changelog.ifexists replace
git config trailer.changelog.command "changed"
```

```shell
git config trailer.jiraref.key "JIRA-Ref"
git config trailer.jiraref.ifexists replace
git config trailer.jiraref.command "echo https://jira.company.com/secure/\$ARG"
```

```shell
git config trailer.tracker_yandex.key "INLMS-"
git config trailer.tracker_yandex.ifmissing doNothing
git config trailer.tracker_yandex.ifexists replace
git config trailer.tracker_yandex.command "echo https://tracker.yandex.ru/\$ARG"
```

```shell
git config trailer.sign.key "Signed-off-by: "
git config trailer.sign.ifmissing add
git config trailer.sign.ifexists doNothing
git config trailer.sign.command 'echo "$(git config user.name) <$(git config user.email)>"'
```

```shell
echo "subject" | git interpret-trailers --trailer fix=42

git config trailer.fix.key "Fix #"
git config trailer.separators ":#"
```

# Настройте трейлер see с помощью команды, чтобы показать тему связанной фиксации и показать, как она работает:

```shell
git config trailer.see.key "See-also: "
git config trailer.see.ifExists "replace"
git config trailer.see.ifMissing "doNothing"
git config trailer.see.command "git log -1 --oneline --format=\"%h (%s)\" --abbrev-commit --abbrev=14 \$ARG"
```


```text
<Commit message subject>
<Commit message description>

Changelog: changed
MR: https://gitlab.com/foo/bar/-/merge_requests/123
```

----

https://www.conventionalcommits.org/en/v1.0.0/

# The commit message should be structured as follows:

```text
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]

Changelog:
```


- https://www.philipp-doblhofer.at/en/blog/automatic-changelog-and-versioning-with-git/
- https://gitlab.com/philipp.doblhofer/automatic-changelog-demo


```shell
curl --location --request GET 'https://gitlab.com/api/v4/projects/30122411/repository/changelog?version=v1.6.0' \
--header 'PRIVATE-TOKEN: W2BGXJj6q7yWs-xbg7He' \
--header 'Cookie: _gitlab_session=e0fa9a7f943cf663a70a498d08310f7f; experimentation_subject_id=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkltVTNORFEwTnpnMExXVXhOekV0TkRFME9TMDRObUUyTFRJeE9UWXdPR1kwTnpnM1lpST0iLCJleHAiOm51bGwsInB1ciI6ImNvb2tpZS5leHBlcmltZW50YXRpb25fc3ViamVjdF9pZCJ9fQ%3D%3D--2e2aab187e8d82c226725cb070ed1e2e9731ea83'

```

```shell
curl --location --request POST 'https://gitlab.com/api/v4/projects/30122411/repository/changelog?version=v1.6.0&from=v1.5.0&branch=main' \
--header 'PRIVATE-TOKEN: W2BGXJj6q7yWs-xbg7He' \
--header 'Cookie: _gitlab_session=e0fa9a7f943cf663a70a498d08310f7f; experimentation_subject_id=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkltVTNORFEwTnpnMExXVXhOekV0TkRFME9TMDRObUUyTFRJeE9UWXdPR1kwTnpnM1lpST0iLCJleHAiOm51bGwsInB1ciI6ImNvb2tpZS5leHBlcmltZW50YXRpb25fc3ViamVjdF9pZCJ9fQ%3D%3D--2e2aab187e8d82c226725cb070ed1e2e9731ea83'
```