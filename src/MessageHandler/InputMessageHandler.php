<?php

namespace App\MessageHandler;

use App\Message\EmailMessage;
use App\Message\InputMessage;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class InputMessageHandler implements MessageSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
        $this->logger = new NullLogger();
    }

    public function __invoke(InputMessage $message)
    {
        $this->logger->info(sprintf('input message: <info>%s > %s</info>', $message->getProvider(), $message->getRecipient()));

        $stamps = [];

        switch ($message->getProvider()) {
            case 'email':
                $message = new EmailMessage(
                    $message->getRecipient()
                );
                break;
        }

        $this->bus->dispatch($message, $stamps);
    }

    public static function getHandledMessages(): iterable
    {
        yield InputMessage::class;
    }
}