<?php

namespace App\MessageHandler;

use App\Message\EmailMessage;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use RuntimeException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;
use Symfony\Component\Mime\Email;
use Throwable;

final class EmailMessageHandler implements MessageSubscriberInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
        $this->logger = new NullLogger();
    }

    public function __invoke(EmailMessage $email)
    {
        $this->logger->info(sprintf('EmailMessageHandler: Sending an email to <info>%s</info>', $email->getRecipient()));

        $email = (new Email())
            ->from('hello@example.com')
            ->to($email->getRecipient())
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(EmailMessage::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        try {
            $this->mailer->send($email);
        } catch (Throwable $exception) {
            $this->logger->error(sprintf(
                'EmailMessageHandler: <error>%s</error>',
                $exception->getMessage()
            ));

            throw new RuntimeException(
                $exception->getMessage(),
                0,
                $exception
            );
        }
    }

    public static function getHandledMessages(): iterable
    {
        yield EmailMessage::class;
    }
}