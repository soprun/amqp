<?php

namespace App\Message;

final class InputMessage
{
    private string $recipient;
    private string $transport;
    private string $provider;
    private string $template;
    private ?string $content;
    private array $parameters;

    public function __construct(
        string $transport,
        string $provider,
        string $template,
        string $recipient,
        array  $parameters,
        ?string $content = null
    )
    {
        $this->recipient = $recipient;
        $this->transport = $transport;
        $this->provider = $provider;
        $this->template = $template;
        $this->content = $content;
        $this->parameters = $parameters;
    }

    public function getTransport(): string
    {
        return $this->transport;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getRecipient(): string
    {
        return $this->recipient;
    }
}
