<?php

namespace App\Message;

final class EmailMessage
{
    private string $recipient;

    public function __construct(string $recipient)
    {
        $this->recipient = $recipient;
    }

    public function getRecipient(): string
    {
        return $this->recipient;
    }
}
