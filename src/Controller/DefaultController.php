<?php

namespace App\Controller;

use App\Message\EmailMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\MessageBusInterface;

final class DefaultController extends AbstractController
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function index(): Response
    {
        $this->bus->dispatch(
            new EmailMessage('lol@mail.com', [
                new AmqpStamp('custom-routing-key', AMQP_NOPARAM, []),
            ])
        );

        return $this->json(['status' => 'ok'], Response::HTTP_ACCEPTED);
    }
}