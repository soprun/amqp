<?php
declare(strict_types=1);

namespace App\Command;

use App\Message\InputMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendEmailCommand extends Command
{
    protected static $defaultName = 'app:send:email';

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();

        $this->bus = $bus;
    }

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // ... put here the code to create the user

        while (true) {
            $now = time();

            $this->bus->dispatch(new InputMessage(
                'amqp',
                'email',
                'blank',
                "{$now}@mail.com",
                [],
                null
            ));

            $output->writeln("recipient: {$now}@mail.com");

            sleep(1);
        }

        // outputs multiple lines to the console (adding "\n" at the end of each line)
//        $output->writeln([
//            'User Creator',
//            '============',
//            '',
//        ]);

        // the value returned by someMethod() can be an iterator (https://secure.php.net/iterator)
        // that generates and returns the messages with the 'yield' PHP keyword
//        $output->writeln($this->someMethod());

        // outputs a message followed by a "\n"
//        $output->writeln('Whoa!');

        // outputs a message without adding a "\n" at the end of the line
//        $output->write('You are about to ');
//        $output->write('create a user.');

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }
}